﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{

    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    // shown when player falls off platform
    public GameObject loseTextObject;
    
    // play again button
    public GameObject playAgain;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;
    
    // adding jump
    public LayerMask groundLayers;
    public float jumpForce = 7;
    public SphereCollider col;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        // adding jump
        col = GetComponent<SphereCollider>();
        // done adding jump
        
        SetCountText();
        winTextObject.SetActive(false);
        loseTextObject.SetActive(false);
        playAgain.SetActive(false);
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 12)
        {
            winTextObject.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed); 

        // adding jump
        if (Input.GetButtonDown("Space") && IsGrounded())
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
        // done adding jump

        if(rb.transform.position.y <= -10)
        {
            loseTextObject.SetActive(true);
            playAgain.SetActive(true);
        }
    }

    // adding jump- won't continuously jump
    private bool IsGrounded()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, groundLayers);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp")) 
        {
            other.gameObject.SetActive(false);
            count += 1;

            SetCountText();
        }
    }

    public void playAgainClicked()
    {
        SceneManager.LoadScene("MiniGame");
    }
}
